package com.example.diplomprogect.adapters;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.diplomprogect.R;


import com.example.diplomprogect.api.RetrofitClient;
import com.example.diplomprogect.models.Chat;
import com.example.diplomprogect.models.Files;
import com.example.diplomprogect.models.Message;
import com.example.diplomprogect.storage.SharedPrefManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ChatViewHolder>{
    private Activity mCtx;
    private SharedPrefManager s;
    public ArrayList<Message> chatList = new ArrayList<>();
    public Chat nickname;

    public MessageAdapter(Activity mCtx) {
        this.mCtx = mCtx;
        this.s = SharedPrefManager.getInstance(mCtx);
    }

    @NonNull
    @Override
    public MessageAdapter.ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.chatitem, parent, false);

        return new MessageAdapter.ChatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageAdapter.ChatViewHolder holder, int position) {
        final Message chat = chatList.get(position);


if(nickname.getId().equals(chat.getReciver())) {
    holder.t1.setText("вы");
}
else
{
    holder.t1.setText(nickname.getNickname());
}
        holder.t2.setText(chat.getCreatedAt());
String text=chat.getMessage();

        if(!chat.getFiles().isEmpty()) {
            final Files file = chat.getFiles().get(0);
            text += "\n" + file.getOriginalName() + " size " + file.getSize();
            holder.download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCtx.checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        DownloadManager downloadmanager = (DownloadManager) mCtx.getSystemService(Context.DOWNLOAD_SERVICE);
                        String url = RetrofitClient.BASE_URL + "/att/?token=" + s.getToken() + "&id=" + file.getFile();
                        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                        request.setTitle("Файл " + file.getOriginalName());
                        request.setDescription("Загрузка");
                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                        File out = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), file.getOriginalName());
                        request.setDestinationUri(Uri.fromFile(out));
                        downloadmanager.enqueue(request);
                    } else {
                        ActivityCompat.requestPermissions(mCtx, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                    }
                }
            });
            holder.download.setVisibility(View.VISIBLE);
        } else {
            holder.download.setVisibility(View.GONE);
        }
        holder.t3.setText(text);
      /*  if(!chat.getFiles().isEmpty()) {
            TextView t  = new TextView(mCtx);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

           t.setText(chat.getFiles().get(0).getOriginalName()+" size "+chat.getFiles().get(0).getSize());
           t.setBackgroundColor(Color.GREEN);
           holder.l.addView(t,lp);

        }*/
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    class ChatViewHolder extends RecyclerView.ViewHolder{

        TextView t1, t2,t3;
        Button download;


        public ChatViewHolder(@NonNull View itemView) {
            super(itemView);

            t1 = itemView.findViewById(R.id.textView3);
            t2 = itemView.findViewById(R.id.textView4);
            t3 = itemView.findViewById(R.id.textView5);
            download = itemView.findViewById(R.id.btn_download);



        }
    }
}
