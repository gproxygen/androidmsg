package com.example.diplomprogect.api;

import android.util.Log;

import com.example.diplomprogect.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    public static final String BASE_URL = "https://sign10.herokuapp.com";
    private static RetrofitClient mInstance;
    private Retrofit retrofit;

    private RetrofitClient(){
        retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(new OkHttpClient.Builder()
                .addInterceptor(
                    new HttpLoggingInterceptor(new LogInterceptor())
                        .setLevel(HttpLoggingInterceptor.Level.BODY)
                ).build())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();
    }

    public static synchronized RetrofitClient getInstance(){
        if (mInstance == null){
          mInstance = new RetrofitClient();
        }
        return mInstance;
    }
    public Api getApi(){
        return retrofit.create(Api.class);
    }

    static class LogInterceptor implements HttpLoggingInterceptor.Logger {

        @Override
        public void log(String message) {
            if (BuildConfig.DEBUG) {
                Log.d("NETWORK", message);
            }
        }
    }
}
