package com.example.diplomprogect.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.Toast;

import com.example.diplomprogect.R;
import com.example.diplomprogect.adapters.ChatAdapter;
import com.example.diplomprogect.adapters.MessageAdapter;
import com.example.diplomprogect.api.RetrofitClient;
import com.example.diplomprogect.models.CallChat;

import com.example.diplomprogect.models.ChResponse;
import com.example.diplomprogect.models.Message;
import com.example.diplomprogect.storage.SharedPrefManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_SHORT;

public class ListChatActivity extends AppCompatActivity {
    private CompositeDisposable disposable = new CompositeDisposable();
    private List<Message> messages = new ArrayList<>();
    RecyclerView r;
    EditText e;
    String id;
    MessageAdapter m;
    boolean t=false;
    InputStream inputStream;
    String type;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_chat);
        e=findViewById(R.id.post);
        Intent i=getIntent();
        id=i.getStringExtra("id");
        r=findViewById(R.id.chlist);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setReverseLayout(true);
        r.setLayoutManager(linearLayoutManager);
        m = new MessageAdapter(ListChatActivity.this);
        r.setAdapter(m);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void onStart() {
        super.onStart();
        disposable.add(Observable.interval(0, 1, TimeUnit.SECONDS)
                .flatMap(aLong -> {
                    messages.clear();
                    return RetrofitClient
                            .getInstance().getApi().getR(id, SharedPrefManager.getInstance().getToken(),"1")
                            .subscribeOn(Schedulers.io());
                })
                .flatMap(data -> {
                    int count = Integer.parseInt(data.getPages());
                    ArrayList requests = new ArrayList();
                    requests.add(Observable.just(data));
                    for (int i = 1; i <= count; i++) {
                        requests.add(RetrofitClient
                                .getInstance().getApi().getR(id, SharedPrefManager.getInstance().getToken(), String.valueOf(i))
                                .subscribeOn(Schedulers.io()));
                    }
                    requests.add(Observable.just(Boolean.TRUE));
                    return Observable.concat(requests);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    if (response instanceof Boolean) {
                        Collections.sort(messages, (u1, u2) -> u2.getCreatedAt().compareTo(u1.getCreatedAt()));
                        m.chatList.clear();
                        m.chatList.addAll(messages);
                        m.notifyDataSetChanged();
                    } else {
                        CallChat data = (CallChat) response;
                        m.nickname = data.getChat();
                        messages.addAll(data.getList());
                    }
                }, error -> {}));
    }



    public void upload(View v)
    {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        try {
            startActivityForResult(intent, 10);
            t=true;
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10) {
            if (resultCode == RESULT_OK) {
                try {
                    inputStream = getContentResolver().openInputStream(Objects.requireNonNull(data.getData()));
                    type = getFileExtension(data.getData());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream byteBuff = new ByteArrayOutputStream();

        int buffSize = 1024;
        byte[] buff = new byte[buffSize];

        int len = 0;
        while ((len = is.read(buff)) != -1) {
            byteBuff.write(buff, 0, len);
        }

        return byteBuff.toByteArray();
    }

    private String getFileExtension(Uri uri) {
        ContentResolver cr = this.getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cr.getType(uri));
    }

    private void sendUploadRequest(byte[] bytes , final String type) {
        Call<ChResponse> call=null;

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), bytes);

        final MultipartBody.Part file = MultipartBody.Part.createFormData("documents", "myImage." + type, requestFile);

        call = RetrofitClient
                .getInstance().getApi().getResponse(String.valueOf(id), SharedPrefManager.getInstance().getToken(), file, e.getText().toString());

        call.enqueue(new Callback<ChResponse>() {
            @Override
            public void onResponse(Call<ChResponse> call, Response<ChResponse> response) {
                ChResponse data = response.body();

                Toast.makeText(getApplicationContext(),String.valueOf(data.isStatus()), LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<ChResponse> call, Throwable t) {
                Log.d("teatimes","fals5443e");
            }



        });



        e.setText("");

    }
    private void sendUploadRequest() {
        Call<ChResponse> call=null;

        call = RetrofitClient
                .getInstance().getApi().getResponse(String.valueOf(id), SharedPrefManager.getInstance().getToken(), e.getText().toString());


        call.enqueue(new Callback<ChResponse>() {
            @Override
            public void onResponse(Call<ChResponse> call, Response<ChResponse> response) {
                ChResponse data = response.body();

                Toast.makeText(getApplicationContext(),String.valueOf(data.isStatus()), LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<ChResponse> call, Throwable t) {
                Log.d("teatimes","fals5443e");
            }



        });



        e.setText("");

    }
    public void send(View v) throws IOException {


        if(t) {
            sendUploadRequest(getBytes(inputStream), type);
            t=false;
        }
        else
        {
            sendUploadRequest();
        }


    }

    @Override
    protected void onStop() {
        disposable.clear();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }
}
